import tippy, {followCursor} from 'tippy.js';
import Swiper from 'swiper';

import 'tippy.js/dist/tippy.css';
import 'tippy.js/dist/backdrop.css';
import 'tippy.js/animations/shift-away.css';
import '../css/style.css';
import 'swiper/css';

const langDisplay = function() {
  document.querySelectorAll
  const $langButton = document.querySelectorAll('.language-button')
  
  const langSelect = {
    remind() {
      this.timeoutID = undefined;
    },
    setup ($langButton) { 
      const $langList = $langButton.nextSibling.nextElementSibling
      if (typeof this.timeoutID === "number") {
        this.cancel();
      }
      if($langList.dataset.state === 'true') {
        $langList.classList.remove('show')
        this.timeoutID = setTimeout(()=>{$langList.dataset.state = false}, 310);
      } else {
        $langList.dataset.state = true
        this.timeoutID = setTimeout(() => {
          $langList.classList.add('show');
        }, 100);
      }
    },
    cancel() {
      clearTimeout(this.timeoutID);
    },
  }
  
  $langButton.forEach(function($ele) {
    if ($ele) $ele.addEventListener("click", () => langSelect.setup($ele))
  });
} 
const swiper = function() {
  new Swiper(".plan-swiper", {
    slidesPerView: 1.2,
    spaceBetween: 15,
    autoHeight: false,
    centerInsufficientSlides: true,
    slidesOffsetAfter: 15,
    slidesOffsetBefore: 15,
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 20,
        autoHeight: true,
      },
      960: {
        slidesPerView: 3,
        spaceBetween: 30,
        autoHeight: true,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 30,
        autoHeight: true,
      },
      1600: {
        slidesPerView: 5,
        spaceBetween: 30,
        autoHeight: true,
      }
    }
  });
  new Swiper(".branch-swiper", {
    slidesPerView: 1.2,
    spaceBetween: 15,
    autoHeight: true,
    centerInsufficientSlides: true,
    breakpoints: {
      640: {
        slidesPerView: 2.3,
        spaceBetween: 20,
        autoHeight: true,
      },
      960: {
        slidesPerView: 2.8,
        spaceBetween: 30,
        autoHeight: true,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 30,
        autoHeight: true,
      }
    }
  });
  
  const serviceSwiper = new Swiper(".service-swiper", {
    slidesPerView: 1,
    spaceBetween: 15,
    allowTouchMove: false,
    autoHeight: true,
  });
  const resetServiceButton = function() {
    document.querySelectorAll('.service-item').forEach(ele => {
      const serviceButton = ele.querySelectorAll('.service-button')
      serviceButton.forEach(button => {
        button.classList.remove('active')
      });
    });
  }
  const $baseIntroduction = document.querySelector('.base-introduction')
  
  if ($baseIntroduction) $baseIntroduction.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[0].slideTo(0, 0);
  });
  
  const $baseItems = document.querySelector('.base-items')
  
  if ($baseItems) $baseItems.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[0].slideTo(1, 0);
  });
  
  const $interIntroduction =document.querySelector('.inter-introduction')
  
  if ($interIntroduction) $interIntroduction.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[1].slideTo(0, 0);
  });
  
  const $interItems = document.querySelector('.inter-items')
  
  if ($interItems) $interItems.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[1].slideTo(1, 0);
  });
  
  const $businessIntroduction = document.querySelector('.business-introduction')
  
  if ($businessIntroduction) $businessIntroduction.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[2].slideTo(0, 0);
  });
  
  const $businessItems = document.querySelector('.business-items')
  
  if ($businessItems) $businessItems.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[2].slideTo(1, 0);
  });
  
  const $businessMethod = document.querySelector('.business-method')
  
  if ($businessMethod) $businessMethod.addEventListener('click', e => {
      e.preventDefault();
      resetServiceButton()
      e.target.classList.add('active')
      serviceSwiper[2].slideTo(2, 0);
    });
}

window.onload = function() {
  langDisplay();
  tippy('[data-tippy-content]', {
    followCursor: true,
    plugins: [followCursor],
  });
  swiper();
};