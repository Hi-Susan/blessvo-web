const path = require('path') 
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
// 清除目錄插件
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// 資料搬移插件
const CopyPlugin = require('copy-webpack-plugin');
const fs = require('fs');
const Dotenv = require('dotenv-webpack');


const createPugHtmlLoaderOptions = (customData = {}) => {
  const options = {
    data: {
      ...customData,
    },
    pretty: true,
    globals: [],
  };
  return JSON.stringify(options);
};

module.exports = (env, options) => {
  const variable = {
    BASE_URL: env.BASE_URL || ''
  }
  global.rootPath = () => '/src';
  global.fullYear = () => new Date().getFullYear();
  global.importI18nJSON = (lang) => {
    const jsonPath = path.resolve(__dirname, `./src/locales/${lang}.json`);
    return JSON.parse(fs.readFileSync(jsonPath));
  };
  const config = {
    entry: './src/js/index.js',
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'bundle.js',
      assetModuleFilename: 'images/[hash][ext][query]'
    },
    module: {
      rules: [
        {
          test: /\.(js)$/,
          include: path.resolve(__dirname, 'src'),
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        {
          test: /\.(css)$/,
          include: [path.resolve(__dirname, 'src'), /node_modules/],
          use: ['style-loader', 'css-loader', 'postcss-loader'],
        },
        {
          test: /\.pug$/,
          use: [
            {
              loader: 'html-loader',
              options: {minimize: false}
              // 不壓縮 HTML
            },
            {
              loader: 'pug-html-loader',
              options: {
                pretty: true, // 美化 HTML 的編排 (不壓縮HTML的一種)
                globals: [], // pug global 函式
                data: {
                  test: '測試'
                }
              }
            }
          ]
        },
        {
          test: /\.(png|jpg|gif)$/i,
          type: 'asset/resource',
          parser: {
            dataUrlCondition: {
              maxSize: 8192
            }
          }
        },
        {
          test: /\.svg/,
          type: 'asset/inline'
        }
      ]
    },
    plugins: [
      // 環境變數
      new Dotenv(),
      // 每次先清除前一次 build 的資料
      new CleanWebpackPlugin(),
      // 搬移靜態檔案
      new CopyPlugin({
        patterns: [
          {from: 'src/static', to: 'static'},
        ]
      }),
      new HtmlWebpackPlugin({
        template: `!!html-loader!pug-html-loader?${createPugHtmlLoaderOptions({LANG: 'zh-tw', ...variable})}!src/pug/home.pug`,
        filename: 'index.html',
        minify: false,
      }),
      new HtmlWebpackPlugin({
        template: `!!html-loader!pug-html-loader?${createPugHtmlLoaderOptions({LANG: 'zh-tw', ...variable})}!src/pug/policy.pug`,
        filename: 'policy/index.html',
        minify: false,
      }),
      new HtmlWebpackPlugin({
        template: `!!html-loader!pug-html-loader?${createPugHtmlLoaderOptions({LANG: 'zh-tw', ...variable})}!src/pug/terms.pug`,
        filename: 'terms/index.html',
        minify: false,
      })
    ],
    devServer: {
      historyApiFallback: true,
      port: 2031,
      hot: true,
      host: '0.0.0.0',
    },
  }
  return config;
}